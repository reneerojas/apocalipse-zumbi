package controllers

import models.Walkers
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.mvc._

/**
 * Created by renee on 23/02/2016.
 */
class Characters extends Controller{

  def listWalkers = Action.async { Request =>
    val walkers = scala.concurrent.Future {Json.toJson(Walkers.walkerList)}
    walkers.map(b=>Ok(b))
  }


}
