package controllers

import models._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.api.mvc.{Action, Controller}
import utils.{Search, Parser}
import play.api.Play.current
import play.api.i18n.Messages.Implicits._

import scala.concurrent.Future

/**
  * Created by renee on 06/06/2015.
  */
class Mapas extends Controller{

  def index = Action.async { implicit request =>
    val m = scala.concurrent.Future {processMapa(Mapa.mapa)}
    m.map(m=>Ok(views.html.index(Mapa.mapForm, m)))
  }

  def initialText = Action.async { implicit request =>
    Future.successful(Ok(Mapa.mapa))
  }

  def base = Action.async{ implicit request =>
    val mapa = scala.concurrent.Future {processMapa(Mapa.mapa)}
    mapa.map(m=>Ok(views.html._mapa.render(m)))
  }

  def build = Action.async{ implicit request =>
    Mapa.mapForm.bindFromRequest.fold(
      formWithErrors => {
        val m = scala.concurrent.Future {processMapa(Mapa.mapa)}
        m.map(m=>BadRequest(views.html._mapa.render(m)))
      },
      success = mapaData => {
        val m = scala.concurrent.Future {processMapa(mapaData.mapa)}
        m.map(m=>Ok(views.html._mapa(m)))
      }
    )
  }

  def pathFind = Action.async{ implicit request =>
    Mapa.mapForm.bindFromRequest.fold(
      formWithErrors => {
        val m = scala.concurrent.Future {processMapa(Mapa.mapa)}
        m.map(m=>BadRequest(views.html._mapa.render(m)))
      },
      success = mapaData => {
        val m = scala.concurrent.Future {Search.aStar(processMapa(mapaData.mapa))}
        m.map(m=>
          Ok(Json.obj(
            "path" -> Json.toJson(m),
            "delay"->m.foldLeft(0)(_+_.delay)
          ))
        )
      }
    )
  }

  def processMapa(m:String) : List[Ponto] = {
    Parser.parseMapa(m.replaceAll("[\\r\\n]","").trim,0)
  }

  def walk (s:String) : List[Ponto] = {
    val mapa = processMapa(s)
    mapa
  }

}
