require.config
  waitSeconds: 60
  paths:
    jquery: '/assets/lib/jquery/jquery'
    bootstrap: '/assets/lib/bootstrap/js/bootstrap'
    tooltip: '/assets/lib/bootstrap/js/tooltip'
    popover: '/assets/lib/bootstrap/js/popover'
    knockout: "/assets/lib/knockout/knockout"
    mapping: "/assets/lib/knockout.mapping"

  shim:
    jquery:
      exports: '$'
    bootstrap:
      deps: ['jquery']
    popover:
      deps: ['bootstrap']
    knockout:
      deps: ["bootstrap","jquery"]
    mapping:
      deps: ["knockout"]

define ['jquery','bootstrap','knockout','mapping','popover'], ($, bootstrap,ko,mapping) ->

  class ApocalipseModel
    constructor: ->
      @walkers = ko.observableArray([])
      @mapa = ko.observable()
      @mapaBase = ko.observable()
      @working = ko.observable(true)

    moveUp: (walker) =>
      walker.y = walker.y+1
      console.log(walker.y+1)

    moveDown: (walker) =>
      walker.y = walker.y-1
      console.log(walker.y+1)

    moveLeft: (walker) =>
      walker.x = (walker.x-1)
      console.log('x: '+ walker.x-1)

    moveRight: (walker) =>
      walker.x = (walker.x+1)
      console.log('x: '+ walker.x+1)

    getWalkers: =>
      @working(true)
      $.ajax "/walkers",
        type: 'GET'
        dataType: 'html'
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) =>
          @walkers.removeAll()
          for w in JSON.parse(data)
            walker = {}
            walker.nome = ko.observable(w.nome)
            walker.imagem = ko.observable(w.imagem)
            walker.x = ko.observable(w.x)
            walker.y = ko.observable(w.y)
            @walkers.push(w)
          @working(false)

    getInitialText: =>
      $("#cost-holder").hide()
      $.ajax "/mapa/texto",
        type: 'GET'
        dataType: 'html'
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) =>
          @mapa(data)
          @updateMap()

    updateMap: =>
      $("#cost-holder").hide()
      @working(true)
      $.ajax "/mapa/rebuild",
        type: 'POST'
        data: 'mapa='+@mapa()
        dataType: 'html'
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) =>
          $('#mapa-holder').html(data)
          @working(false)

    pathFind: =>
      @working(true)
      $.ajax "/mapa/pathFind",
        type: 'POST'
        data: 'mapa='+@mapa()+'&walkers='+ko.toJSON(@walkers)
        dataType: 'html'
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) =>
          res = JSON.parse data
          $("#cost-holder").show()
          for p in res.path
            $("#ponto-"+p.pos).addClass("escolhido")

          $("#cost-value").html(res.delay)
          @working(false)

    listWalkers: => console.log(@walkers())


  $ ->
    $("#cost-holder").hide()
    @zumbi = new ApocalipseModel()
    @zumbi.getWalkers()
    @zumbi.getInitialText()
    ko.applyBindings(@zumbi)
    $('[data-toggle="popover"]').popover()
