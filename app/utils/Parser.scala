package utils

import models._
import play.api.Logger

import scala.collection.immutable.Nil

/**
 * Created by renee on 07/02/2016.
 */
object Parser {

  def parseMapa (m:String, pos:Int) : List[Ponto] =  {
    if (m.isEmpty) return Nil
    val s = parseSolo(m.head,m.tail.take(1), pos)

    s match {
      case null => parseMapa(m.tail,pos)
      case _ => if(m.tail.isEmpty){
        s::Nil
      } else {
        s::parseMapa(m.tail,pos+1)
      }
    }
  }

  def parseSolo (s:Char, nextTwo:String, pos:Int) : Ponto = {
    val y:Int = pos / Mapa.squaredSize
    val x:Int = pos % 42
    //Logger.debug("letter: "+s.toString+" pos:"+pos +" x:"+x+" y:"+y)
    val pt:Ponto = s match {
      case 'a' | 't' | 'g' | 'p' | 'b' => Terreno(x,y,pos,Mapa.solos.filter(_.letra == s).head)
      case _ => null
    }
    pt
  }

}
