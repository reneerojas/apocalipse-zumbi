package utils

import models.{Mapa, Terreno, Personagem, Ponto}


/**
 * Created by renee on 07/02/2016.
 */
object Distance {

  def XYtoFinish(candidate: Ponto) :Int = XtoPoint(candidate,Mapa.finish) + YtoPoint(candidate,Mapa.finish)

  def XYdistance(candidate: Ponto, dest: Ponto) :Int  = dest match {
    case d:Terreno => XYtoTerrain(candidate,d)
    case d:Personagem => XYtoHouse(candidate,d)
  }

  def XYtoTerrain(candidate: Ponto, dest: Terreno) :Int = XtoPoint(candidate,dest) + YtoPoint(candidate,dest)

  def XYtoHouse(candidate:Ponto, next: Personagem): Int = XtoHouse(candidate,next) + YtoHouse(candidate,next)

  def XtoPoint(candidate: Ponto, dest: Terreno): Int = candidate match {
    case c:Personagem => distance(c.x, dest.x)
    case c:Terreno => distance(c.x, dest.x)
  }

  def YtoPoint(candidate: Ponto, dest: Terreno): Int = candidate match {
    case c:Personagem => distance(c.y, dest.y)
    case c:Terreno => distance(c.y, dest.y)
  }

  def XtoHouse(candidate: Ponto, next: Personagem): Int = candidate match {
    case c:Personagem => distance(c.x, next.x)
    case c:Terreno => distance(c.x, next.x)
  }

  def YtoHouse(candidate: Ponto, next: Personagem): Int = candidate match {
    case c:Personagem => distance(c.y, next.y)
    case c:Terreno => distance(c.y, next.y)
  }

  def distance(from:Int,to:Int) :Int = Math.abs(from - to)
}
