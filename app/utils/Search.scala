package utils

import models._
import play.Logger

import scala.collection.mutable
import scala.collection.mutable.Stack

/**
  * Created by renee on 01/03/2016.
  */

object Search {

  def aStar(mapa: List[Ponto]) :List[Walk] = {
    val rick = Walkers.walkerList.filter(p=> p.nome == "Rick").head
    val walkers = Walkers.walkerList.filter(p=> p.nome != "Rick").sortBy(_.y)

    //Logger.debug("from: "+rick+ " to: "+walkers.head)

    var current = rick
    var full_walk:List[Walk] = Nil

    walkers.foreach{ next =>
      full_walk = full_walk ::: walkerToWalker(current,next,mapa)
      current = next
    }
    full_walk ::: aStarSearch(walkerAsPoint(current,mapa),findFinish(mapa),mapa)
  }

  def walkerToWalker(walker_from: Walker, walker_to: Walker,mapa: List[Ponto]) : List[Walk]= {
    aStarSearch(walkerAsPoint(walker_from,mapa),walkerAsPoint(walker_to,mapa),mapa)
  }

  def walkerAsPoint(walker: Walker,mapa: List[Ponto]) :Terreno = {
    findWalker(walker,mapa).asInstanceOf[Terreno]
  }

  def aStarSearch(start: Terreno, end: Terreno, mapa: List[Ponto]) :List[Walk] = {

    var frontier = new mutable.PriorityQueue[(Ponto,Int)]()(Ordering.by(-_._2))
    var cost_so_far = new Stack[(Ponto,Int)]()
    var came_from = new Stack[(Ponto,Ponto)]()

    frontier.enqueue((start,0))
    cost_so_far.push((start,0))
    came_from.push((start,null))

    while(frontier.nonEmpty){
      //Logger.debug("Frontier: "+frontier.take(5).toString())
      val current = frontier.dequeue()
      // Logger.debug("current: "+current.toString())

      //se for o final, limpa as fronteiras

      if(current._1.asInstanceOf[Terreno].y == end.y && current._1.asInstanceOf[Terreno].x == end.x) {
        //walkerCheck+=1
        frontier.clear()
        //Logger.debug("!walker Check! "+ current._1)
        //frontier.enqueue((findWalker(next_walker,mapa),0))
      }
      else {
        findNeigbors(current._1,mapa).foreach(n => {
          // Logger.debug(current.toString())

          //se existir distancia calculada para o ponto atual, pega o valor
          var so_far_current = 0
          if (cost_so_far.exists(_._1 == current._1)){
            so_far_current = cost_so_far.find(_._1 == current._1).get._2
          }
          val new_cost = so_far_current + Mapa.delay(n)
          //Logger.debug("so far current "+so_far_current)

          //se existir distancia calculada para o vizinho, pega o valor
          var so_far = 0
          if (cost_so_far.exists(_._1 == n)){
            so_far = cost_so_far.find(_._1 == n).get._2
          }

          // Logger.debug(new_cost +"<"+so_far + "||"+ !cost_so_far.exists(_._1 == n))
          //se o custo ainda nao existir e o novo custo ainda nao existir
          if (!cost_so_far.exists(_._1 == n) || new_cost < so_far){

            //adiciona o custo para o ponto
            cost_so_far.push((n,new_cost))
            //marca deslocamento atual -> vizinho
            came_from.push((current._1,n))

            //adiciona a fronteira
            frontier.enqueue((n, new_cost + heuristicCost(n,end,new_cost)))
            //Logger.debug("NEW frontier "+n.toString +" came from"+current._1+" cost "+(new_cost))
          }
        })
      }
    }
    decodeCame(start, end, came_from.toList)
  }

  def heuristicCost(ponto: Ponto, end: Ponto, so_far: Int): Int = {
    Distance.XYtoTerrain(ponto,end.asInstanceOf[Terreno]) + so_far
  }

  def decodeCame(start: Terreno, end: Terreno, came:List[(Ponto,Ponto)]) :List[Walk] = {
    var current = end
    var walk = new Stack[Walk]()
    //Logger.debug("CAME")
    //Logger.debug(came.toString)
    //came.foreach(c=>{
    //Logger.debug(c.toString())
    //Logger.debug("pointed from "+came.filter(c2 => c2._2 == c._2).toString)
    //
    //  })

    while (current != start){
      val camePoints = came.filter(_._2 == current)
      current = camePoints.head._1.asInstanceOf[Terreno]
     // Logger.debug("c "+current+" - s "+start)
     // Logger.debug("Step "+current)
      walk.push(Mapa.pointWalk(current))
    }
    walk.toList
  }

  def findNeigbors(ponto:Ponto,mapa: List[Ponto]) :List[Ponto] = {
   // Logger.debug("neigbors to "+ponto)
    val m = mapa.filter(p => matchPoint(ponto,p))
    //Logger.debug(m.toString)
    m
  }

  def matchPoint(origem: Ponto, destino: Ponto) :Boolean = destino match {
    case pt:Terreno =>
      if (pt.solo.letra =='b') false
      else isNeigbor(origem, pt.x, pt.y)
  }

  def isNeigbor(origem: Ponto, x: Int, y: Int) :Boolean = origem match {
    case m:Terreno =>
      if (x == m.x-1 && y == m.y
        || x == m.x+1 && y == m.y
        || y == m.y-1 && x == m.x
        || y == m.y+1 && x == m.x) true else false
    case _ => false
  }

  def findWalker(walker: Walker, mapa: List[Ponto]) : Ponto = {
    mapa.filter(p => walker.x == p.asInstanceOf[Terreno].x).filter(p => walker.y == p.asInstanceOf[Terreno].y).head
  }

  def findFinish(mapa: List[Ponto]) : Terreno = {
    mapa.filter(p => Mapa.finish.x == p.asInstanceOf[Terreno].x).filter(p => Mapa.finish.y == p.asInstanceOf[Terreno].y).head.asInstanceOf[Terreno]
  }

  def isFinish(p:Ponto): Boolean ={
    if(p.isInstanceOf[Terreno]){
      p.asInstanceOf[Terreno].y == 41
    } else false
  }
}
