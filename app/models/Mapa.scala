package models

import play.api.data.Form
import play.api.data.Forms._

/**
 * Created by renee on 17/05/2015.
 */
case class MapaText(mapa: String)
case class Solo(letra: Char, nome: String, delay: Int)
case class Terreno(x: Int, y: Int, pos: Int, solo: Solo)  extends Ponto (x, y, pos)
case class Personagem(x: Int, y: Int, pos: Int, walker: Walker) extends Ponto (x, y, pos)

class Ponto(x: Int, y: Int, pos: Int)

class PathDelay(delay: Int)

//case class Ponto(x: Int, y: Int)


object Mapa {
  val maxTime = 720

  //Map size 42X42
  val squaredSize = 42

  val mapForm = Form(
    mapping(
      "mapa" -> text
    )(MapaText.apply)(MapaText.unapply)
  )

  val finish = Terreno(22,41,0,Solo('f',"fim",0))

  val solos = List(
    Solo('f',"fim",0),
    Solo('a',"asfalto",1),
    Solo('t',"terra",3),
    Solo('g',"grama",5),
    Solo('p',"paralelepipedo",10),
    Solo('b',"predio",10000)
  )

  val mapa =
      "gggggggggggggggggggggggggggggggggggggggggg\n" +
      "gggggggggggggggggggggggggggggggggggggggggg\n" +
      "ggbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbgg\n" +
      "ggbggggggggggggggatttagggppppppppppppppbgg\n" +
      "ggbgggaaaaaaaaaaaatttagggpggggggggggggpbgg\n" +
      "ggbgggabbbaabbbagatttaaaapgttttttttttgpbgg\n" +
      "ggbgggabbbbbbbbagatttaaaapgttttttttttgpbgg\n" +
      "ggbgggabbbbbbbbagatttagggpggggggggggggpbgg\n" +
      "ggbgggabbbbbbbbagatttagggppppppppppppppbgg\n" +
      "ggbaaaaaaaaaaaaagatttagggggggggggggggggbgg\n" +
      "ggbapppppppppggggatttagggbbbbbbbbbbggggbgg\n" +
      "ggbapbbbpbbbpggggatttaaaaaaaaaaaaabggggbgg\n" +
      "ggbappbppbpbpaaaaatttaggggbbbbbbbabggggbgg\n" +
      "ggbappbppbbbpaaaaatttaggggbbbbbaaabggggbgg\n" +
      "ggbapbbbpbpbpaaaaatttaggggbbbbbbbpbggggbgg\n" +
      "ggbapppppppppggggatttaaaapppppppppbggggbgg\n" +
      "ggbttttttttttttggatttagggbbbbbbbbbbggggbgg\n" +
      "ggbttttttttttttggatttagggttttttttttttttbgg\n" +
      "ggbttppppppppppggatttagggttttttttttttttbgg\n" +
      "ggbttpbbbbbbbbpggatttagggttbbbbbbbbbbbtbgg\n" +
      "ggbttpbbbbbbpppaaatttaaaattbbbbbbbbbbbtbgg\n" +
      "ggbttpbbbbbbpppaaatttaaaattbbbbbbbbbbbtbgg\n" +
      "ggbttpbbbbbbbbpggatttagggttbbbbbpbbbbbtbgg\n" +
      "ggbttppppppppppggatttagggtttttttpttttttbgg\n" +
      "ggbttttttttttttggatttagggtttttttpttttttbgg\n" +
      "ggbttttttttttttggaaaaaggggggggggpggggggbgg\n" +
      "ggbppppppppppppggaaaaaggggggggggpggggggbgg\n" +
      "ggbppppppppppppggggaggggggggggggpggggggbgg\n" +
      "ggbpaaaaaaaaaaaaaaaagbbbbbbbbaaapaaaaaabgg\n" +
      "ggbpappbbbbbpggggggagbbbbbbaaaaapaaaaaabgg\n" +
      "ggbpappbbbbbpggggggagbbbbbbaaaaapaaaaaabgg\n" +
      "ggbpappbbbbbpgggggaagbbbbbbbbaaapaaaaaabgg\n" +
      "ggbpappaabbbpgggggabggggggtppppppppppppbgg\n" +
      "ggbpappbbbbbpaaaaaabggggggtppppppppppppbgg\n" +
      "ggbpappbbbbbpabbbbbbggggggtttbbbbbbbttpbgg\n" +
      "ggbpappbbbbbpaggggggggggggtttbbbbbbppppbgg\n" +
      "ggbpappppppppaaaaaaaaaaaaatttbbbbbbbttpbgg\n" +
      "ggbpappppppppppppppppaggggtppppppppppppbgg\n" +
      "ggbpaaaaaaaaaaaaaaaapaggggtppppppppppppbgg\n" +
      "ggbbbbbbbbbbbbbbbbbbpabbbbbbbbbbbbbbbbbbgg\n" +
      "ggggggggggggggggggggpagggggggggggggggggggg\n" +
      "ggggggggggggggggggggpagggggggggggggggggggg\n";



  def delay(ponto: Ponto) :Int = ponto match {
    case p:Personagem => 0
    case p:Terreno => p.solo.delay
  }

  def pointCost(ponto:Ponto) :Int = ponto match{
    case p:Personagem => 0
    case p:Terreno => p.solo.delay
  }

  def pointWalk(ponto: Ponto) :Walk = ponto match {
    case p:Personagem => new Walk(p.x , p.y, p.pos, 0)
    case p:Terreno => new Walk(p.x , p.y, p.pos, p.solo.delay)
  }

}