package models

import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Reads, Writes}

/**
 * Created by renee on 07/02/2016.
 */
case class Walk(x: Int, y: Int, position: Int, delay: Int)

object Walk {
  val reader: Reads[Walk] = (
    (JsPath \ "x").read[Int] and
      (JsPath \ "y").read[Int] and
      (JsPath \ "pos").read[Int] and
      (JsPath \ "delay").read[Int]
    )(Walk.apply _)

  val writer: Writes[Walk] = (
    (JsPath \ "x").write[Int] and
      (JsPath \ "y").write[Int] and
      (JsPath \ "pos").write[Int] and
      (JsPath \ "delay").write[Int]
    )(unlift(Walk.unapply))

  implicit val walkFormat: Format[Walk] =
    Format(reader, writer)
}
