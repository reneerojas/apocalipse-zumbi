package models

/**
  * Created by renee on 07/02/2016.
  */
case class Walker(nome: String, imagem: String, x: Int, y: Int)
case class WalkerList(walkers: List[Walker])

import play.api.libs.functional.syntax._
import play.api.libs.json._

object Walker {

  val walkerReads: Reads[Walker] = (
    (JsPath \ "nome").read[String] and
      (JsPath \ "imagem").read[String] and
      (JsPath \ "x").read[Int] and
      (JsPath \ "y").read[Int]
    )(Walker.apply _)

  val walkerWrites: Writes[Walker] = (
    (JsPath \ "nome").write[String] and
      (JsPath \ "imagem").write[String] and
      (JsPath \ "x").write[Int] and
      (JsPath \ "y").write[Int]
    )(unlift(Walker.unapply))

  implicit val walkerFormat: Format[Walker] =
    Format(walkerReads, walkerWrites)
}


object Walkers {

  val walkerList = List(
    Walker("Rick","rick.png",12,20),
    Walker("Carl","carl.png",32,5),
    Walker("Daryl","daryl.png",35,35),
    Walker("Glen","glen.png",8,32),
    Walker("Maggie","maggie.png",31,13)
  )

  def nextWalker(count:Int) :Walker = {
    walkerList.filterNot(w => w.nome == "Rick").sortBy(_.y).drop(count).head
  }

  def hasNextWalker(count:Int) :Boolean ={
    count <= walkerList.length
  }
}
